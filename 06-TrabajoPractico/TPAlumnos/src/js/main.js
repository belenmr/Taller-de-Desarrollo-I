var usuarioInput = {}
usuarioInput.username;
usuarioInput.pass;


function login() {
    usuarioInput.usuario = document.getElementById("idUsuario");
    usuarioInput.clave = document.getElementById("idClave");

    if (validarUsuario(usuarioInput.usuario.value, usuarioInput.clave.value)) {
        alert("Usuario y clave correctas");
        loginOk();
        window.location.href="alumno/vistas/grillaAlumnos.html";
    } else {
        alert("Usuario y/o clave incorrectas");
    }
}


function loginOk() {
    sessionStorage.setItem('LoginOk','OK');
}


function validarUsuario(usuario, clave) {
    let estadoValidacion = false;
    let usuarioList = [];

    if (localStorage.getItem("usuarioStorage")==null){
        estadoInicial(clave);
    }
    usuarioList = JSON.parse(localStorage.getItem("usuarioStorage"));

    usuarioList.forEach(element => {
        if (element.usuario == usuario && element.clave == clave) {
            estadoValidacion = true;     
        }
        });

    return estadoValidacion;
}


function estadoInicial(clave) {
    let primerUsuario = {};
    primerUsuario.usuario = "Admin";
    primerUsuario.clave = clave;
    
    let usuarioList = [];
    usuarioList.push(primerUsuario);
    let usuarioListStr = JSON.stringify(usuarioList);
    localStorage.setItem("usuarioStorage", usuarioListStr);
}


function getLogin(url) {
    var data = sessionStorage.getItem('LoginOk');
    verificarLogin(data, url);
}


function verificarLogin(Data) {
    if (Data != "OK") {
        window.location.href=url;
    }
}