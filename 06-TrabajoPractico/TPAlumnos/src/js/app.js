var app = angular.module("alumnoApp",['ngRoute']);
    app.config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when("/home",{
                templateUrl: "vistas/alumno/grillaAlumnos.html",
                controller: "alumnoController"
            })
            .when("/login",{
                templateUrl: "vistas/usuario/loginForm.html",
                controller: "usuarioController"
            })
            .when("/clave",{
                templateUrl: "vistas/usuario/claveForm.html",
                controller: "usuarioController"
            })
            .when("/altaalumno",{
                templateUrl: "vistas/alumno/altaAlumnoForm.html",
                controller: "alumnoController"
            })
            .when("/bajaalumno",{
                templateUrl: "vistas/alumno/bajaAlumnoForm.html",
                controller: "alumnoController"
            })
            .when("/modificacionalumno",{
                templateUrl: "vistas/alumno/modificacionAlumnoForm.html",
                controller: "alumnoController"
            })
            .when("/escalacalificaciones",{
                templateUrl: "vistas/calificacion/grillaEscalaCalificaciones.html",
                controller: "alumnoController"
            })
            .when("/cargacalificaciones",{
                templateUrl: "vistas/alumno/cargaCalificaionesForm.html",
                controller: "alumnoController"
            })
            .when("/aplazos",{
                templateUrl: "vistas/alumno/grillaAplazos.html",
                controller: "estadoController"
            })
            .when("/promedios",{
                templateUrl: "vistas/alumno/grillaPromedios.html",
                controller: "estadoController"
            })
            .when("/estados",{
                templateUrl: "vistas/estado/grillaEstados.html",
                controller: "estadoController"
            })
            .when("/altaestado",{
                templateUrl: "vistas/estado/altaEstadoForm.html",
                controller: "estadoController"
            })
            .when("/bajaaestado",{
                templateUrl: "vistas/estado/bajaEstadoForm.html",
                controller: "estadoController"
            })
            .when("/modificacionestado",{
                templateUrl: "vistas/estado/modificacionEstadoForm.html",
                controller: "estadoController"
            })
            .otherwise ({
                redirectTo: "/home"
            });
 
        $locationProvider.hashPrefix('');
    });
    
    /*************** Usuario Controller ***************/
    app.controller('usuarioController', function ($scope) {

    /***** Usuario - Inicio de sesion *****/
        $scope.usuariosList = [];

        if (localStorage.getItem("usuarioStorage")) {
            $scope.usuariosList = localStorage.getItem("usuarioStorage");
        } else {
            $scope.usuario = {nombre: "admin",  clave: "123"};
            $scope.usuariosList = [];
            $scope.usuariosList.push($scope.usuario);
            $scope.usuariosStr = JSON.stringify(localStorage.setItem("usuarioStorage", usuariosStr));

            
        }

        $scope.login = function() {
            if(validarUsuario($scope.usuario.nombre,$scope.usuario.clave)){
                alert("Bienvenido");
                loginOk();
                window.location.href="#/home";
            } else {
                alert("Usuario y/o clave incorrectas");
            }
        }

        function validarUsuario(usuario, clave) {
            let estadoValidacion = false;
            let usuarioList = [];
        
            if (localStorage.getItem("usuarioStorage")==null){
                estadoInicial(clave);
            }
            usuarioList = JSON.parse(localStorage.getItem("usuarioStorage"));
        
            usuarioList.forEach(element => {
                if (element.usuario == usuario && element.clave == clave) {
                    estadoValidacion = true;     
                }
                });
        
            return estadoValidacion;
        }
        
        
        function estadoInicial(clave) {
            let primerUsuario = {};
            primerUsuario.usuario = "Admin";
            primerUsuario.clave = clave;
            
            let usuarioList = [];
            usuarioList.push(primerUsuario);
            let usuarioListStr = JSON.stringify(usuarioList);
            localStorage.setItem("usuarioStorage", usuarioListStr);
        }

        $scope.getLogin = function(url){
            $scope.data = sessionStorage.getItem('LoginOk');
            verificarLogin($scope.data);
        };

        function verificarLogin(Data) {
            if (Data != "OK") {
                window.location.href=url;
            }
        }

        /***** Usuario - Cambio de clave *****/
        /***** Usuario - Cierre de sesion *****/              
    });



    /*************** Alumno Controller ***************/
    app.controller('alumnoController', function ($scope) {
        /***** Escala de calificaciones*****/
        $scope.calificacionesList = [];

        if (localStorage.getItem("markStorage")) {
            $scope.calificacionesList = JSON.parse(localStorage.getItem("markStorage"));
        } else {
            $scope.calificacionesList = [
                {nota: "Ausente", condicion: "Desaprobado"},
                {nota: 1, condicion: "Desaprobado"},
                {nota: 2, condicion: "Desaprobado"},
                {nota: 3, condicion: "Desaprobado"},
                {nota: 4, condicion: "Aprobado"},
                {nota: 5, condicion: "Aprobado"},
                {nota: 6, condicion: "Aprobado"},
                {nota: 7, condicion: "Aprobado"},
                {nota: 8, condicion: "Aprobado"},
                {nota: 9, condicion: "Aprobado"},
                {nota: 10, condicion: "Aprobado"}          
            ];

            $scope.calificacionesStr = JSON.stringify($scope.calificacionesList);
            localStorage.setItem("markStorage", $scope.calificacionesStr);
        }

        /***** Alumno - Visualizacion de grilla *****/
        /***** Alumno - Alta *****/
        /***** Alumno - Baja *****/
        /***** Alumno - Modificacion *****/
    });



    /*************** Estado Controller ***************/
    app.controller('estadoController', function ($scope) {

        /***** Estado - Visualizacion de grilla *****/
        if (localStorage.getItem("statusStorage")) {
            $scope.estadosList = localStorage.getItem("statusStorage");
        } else {
            $scope.estadosList = [
                {codigo: "R",descripcion: "Regular"},
                {codigo: "I",descripcion: "Inscripto"},
                {codigo: "C",descripcion: "Cursando"},
                {codigo: "L",descripcion: "Libre"}
            ];

            localStorage.setItem("statusStorage", estadosList);
        }  
        /***** Estado - Alta *****/
        /***** Estado - Baja *****/
        /***** Estado - Modificacion *****/
    });