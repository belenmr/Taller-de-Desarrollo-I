var empleadoInput = {}
empleadoInput.username;
empleadoInput.pass;


function login() {
    empleadoInput.dni = document.getElementById("idUser");
    empleadoInput.clave = document.getElementById("idPass");

    if (validateUser(empleadoInput.dni.value, empleadoInput.clave.value)) {
        alert("Usuario y clave correctas");
        loginOk();
        window.location.href="empleado/vistas/grillaEmpleado.html";
    } else {
        alert("Usuario y/o clave incorrectas");
    }
}

function loginOk() {
    sessionStorage.setItem('LoginOk','OK');
}


function validateUser(dni, clave) {
    let validationStatus = false;
    let empleadoList = [];

    if (localStorage.getItem("empleadoStorage")==null){
        estadoInicial(dni, clave);
    }
    empleadoList = JSON.parse(localStorage.getItem("empleadoStorage"));

    empleadoList.forEach(element => {
        if (element.dni == dni && element.clave == clave) {
            validationStatus = true;     
        }
        });

    return validationStatus;
}

function estadoInicial(dni, clave) {
    let administrador = {};
    administrador.nombre = "Administrador";
    administrador.apellido = "Administrador";
    administrador.dni = dni;
    administrador.domicilio = "Administrador";
    administrador.celular = "";
    administrador.nacimiento = "";
    administrador.clave = clave;
    administrador.puesto = "Administrador";
    
    let empleadoList = [];
    empleadoList.push(administrador);
    let empleadoListStr = JSON.stringify(empleadoList);
    localStorage.setItem("empleadoStorage", empleadoListStr)
    
    let puestoAdministrador = {};
    puestoAdministrador.abreviatura = "ADM";
    puestoAdministrador.puesto = "Administrador";

    let puestoList = [];
    puestoList.push(puestoAdministrador);
    let puestoListStr = JSON.stringify(puestoList);
    localStorage.setItem("puestoStorage", puestoListStr);

}