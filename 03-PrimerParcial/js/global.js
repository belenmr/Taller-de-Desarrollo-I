function getLogin(url) {
    var data = sessionStorage.getItem('LoginOk');
    verifyLogin(data, url);
}


function verifyLogin(Data) {
    if (Data != "OK") {
        window.location.href=url;
    }
}


function cargaPuesto() {
    let select = document.getElementById("idPuesto");
    let puestoList = [];
    let puestoDefault = {};
    
    if (localStorage.getItem("puestoStorage") == null) {
        puestoDefault.abreviatura = "ADM";
        puestoDefault.puesto = "Administrador";
        puestoList.push(puestoDefault);
        let puestoListStr = JSON.stringify(puestoList);
        localStorage.setItem("puestoStorage", puestoListStr);
    }

    puestoList = JSON.parse(localStorage.getItem("puestoStorage"));
    
    puestoList.forEach(element => {
        let option = document.createElement("option");
        option.text = element.puesto;
        select.appendChild(option);  
    });
}


function validarCampoObligatorio(valorCampo) {
    let validacionStatus = true;
    //if (valorCampo != null || valorCampo != " ") {
    //Expresiones regulares /^\s+$/.test(valorCampo): obliga a que el valor introducido por el usuario no sólo esté formado por espacios en blanco
    if( valorCampo == null || valorCampo.length == 0 || /^\s+$/.test(valorCampo) ) { 
        validacionStatus = false;
    }

    return validacionStatus;
}

function logout() {
    sessionStorage.removeItem('LoginOk');
    window.location.href="../../index.html";
    
}