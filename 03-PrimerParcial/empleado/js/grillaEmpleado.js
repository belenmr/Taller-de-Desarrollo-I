let url = "../../index.html";
getLogin(url);

let empleadoList = JSON.parse(localStorage.getItem("empleadoStorage"));

function cargarGrilla() {
    if(localStorage.getItem("empleadoStorage")){
        let tbody = document.getElementById("gridBody");
        //console.log(tbody);
                
        empleadoList.forEach((element,index) => {
            let row = tbody.insertRow(index);
            let cellNombre = row.insertCell(0);
            let cellApellido = row.insertCell(1);
            let cellDni = row.insertCell(2);
            let cellDomicilio = row.insertCell(3);
            let cellCelular = row.insertCell(4);
            let cellNacimiento = row.insertCell(5);
            let cellPuesto = row.insertCell(6);
            let cellClave = row.insertCell(7);
            let cellModify = row.insertCell(8);
            let cellDelete = row.insertCell(9);
            cellNombre.innerHTML = element.nombre;
            cellApellido.innerHTML = element.apellido;
            cellDni.innerHTML = element.dni;
            cellDomicilio.innerHTML = element.domicilio;
            cellCelular.innerHTML = element.celular;
            cellNacimiento.innerHTML = element.nacimiento;
            cellPuesto.innerHTML = element.puesto;
            cellClave.innerHTML = element.clave;
            cellModify.innerHTML = '<input class="btnModify"  type="button" onclick="goToModifyForm('+ index + ')" value="Modificar" />';
            cellDelete.innerHTML = '<input class="btnDelete"  type="button" onclick="eliminarEmpleado('+ index + ')" value="Eliminar" />';
            
        });
        
    } 
    else {
        alert("No hay usuarios registrados");
    }
}



function eliminarEmpleado(posicion) {
    empleadoList.splice(posicion,1);
    localStorage.setItem('empleadoStorage', JSON.stringify(empleadoList));
    
    location.reload();
}

function goToModifyForm(posicion) {
    let respuesta = false;
    let viejoEmpleado = {};
    let viejoEmpleadoList = [];
    
    viejoEmpleado.posicion = posicion;
    viejoEmpleado.nombre = empleadoList[posicion].nombre;
    viejoEmpleado.apellido = empleadoList[posicion].apellido;
    viejoEmpleado.dni = empleadoList[posicion].dni;
    viejoEmpleado.domicilio = empleadoList[posicion].domicilio;
    viejoEmpleado.celular = empleadoList[posicion].celular;
    viejoEmpleado.nacimiento = empleadoList[posicion].nacimiento;
    viejoEmpleado.clave = empleadoList[posicion].clave;
    viejoEmpleado.puesto = empleadoList[posicion].puesto;
    
    viejoEmpleadoList.push(viejoEmpleado);
    let viejoEmpleadoListStr = JSON.stringify(viejoEmpleadoList);
    localStorage.setItem("auxStorage", viejoEmpleadoListStr);
  
    respuesta = window.confirm("¿ Desea modificar el usuario "+ viejoEmpleado.dni + "?");
    
    if (respuesta) {
        window.location.href = "modificarEmpleadoForm.html"; 
    } else {
        localStorage.removeItem("auxStorage");
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    cargarGrilla()
  });