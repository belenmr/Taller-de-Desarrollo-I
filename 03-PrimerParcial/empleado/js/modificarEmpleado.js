let url = "../../index.html";
getLogin(url);

let viejoEmpleado = {};
var empleadoList = JSON.parse(localStorage.getItem("empleadoStorage"));

function cargarModificarEmpleado() {
    let viejoEmpleadoList = [];
    viejoEmpleadoList = JSON.parse(localStorage.getItem("auxStorage"));
   

    viejoEmpleadoList.forEach(element => {
        viejoEmpleado.posicion = element.posicion;
        viejoEmpleado.nombre = element.nombre;
        viejoEmpleado.apellido = element.apellido;
        viejoEmpleado.dni = element.dni;
        viejoEmpleado.domicilio = element.domicilio;
        viejoEmpleado.celular = element.celular;
        viejoEmpleado.nacimiento = element.nacimiento;
        viejoEmpleado.clave = element.clave;
        viejoEmpleado.puesto = element.puesto;

    });
    
    localStorage.removeItem("auxStorage");
    
    document.getElementById("idNombre").value =viejoEmpleado.nombre;
    document.getElementById("idApellido").value =viejoEmpleado.apellido;
    document.getElementById("idDNI").value =viejoEmpleado.dni;
    document.getElementById("idDomicilio").value =viejoEmpleado.domicilio;
    document.getElementById("idCelular").value =viejoEmpleado.celular;
    document.getElementById("idNacimiento").value =viejoEmpleado.nacimiento;
    document.getElementById("idClave").value =viejoEmpleado.clave;
    document.getElementById("idPuesto").value =viejoEmpleado.puesto;  
}

function modificarEmpleado() {
    let posicion = viejoEmpleado.posicion;
    let nombre = document.getElementById("idNombre").value; 
    let apellido = document.getElementById("idApellido").value;
    let domicilio = document.getElementById("idDomicilio").value;
    let celular = document.getElementById("idCelular").value;
    let clave = document.getElementById("idClave").value;
    let puesto = document.getElementById("idPuesto").value;
    
    if (validarCampoObligatorio(nombre) && validarCampoObligatorio(apellido) && validarCampoObligatorio(domicilio) && validarCampoObligatorio(clave) && validarCampoObligatorio(puesto)) {
        empleadoList[posicion].nombre = nombre;
        empleadoList[posicion].apellido = apellido;
        empleadoList[posicion].domicilio = domicilio;
        empleadoList[posicion].celular = celular;
        empleadoList[posicion].clave = clave;
        empleadoList[posicion].puesto = puesto;

        alert("Empleado modificado exitosamente");
    } else {
        alert("Debe completar todos los campos")
    }

    let empleadoListStr = JSON.stringify(empleadoList);

    localStorage.setItem("empleadoStorage", empleadoListStr);

    window.location.href="grillaEmpleado.html";

}

document.addEventListener("DOMContentLoaded", function(event) {
    cargarModificarEmpleado();
    cargaPuesto()
  });