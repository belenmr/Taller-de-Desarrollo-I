let url = "../../index.html";
getLogin(url);

function registrarEmpleado() {
    let nuevoEmpleado = {};
    let empleadoList = [];
    let statusNuevoEmpleado = true;
    let statusCamposObligatorios = false;

    nuevoEmpleado.nombre = document.getElementById("idNombre").value;    
    nuevoEmpleado.apellido = document.getElementById("idApellido").value;
    nuevoEmpleado.dni = document.getElementById("idDNI").value;
    nuevoEmpleado.domicilio = document.getElementById("idDomicilio").value;
    nuevoEmpleado.celular = document.getElementById("idCelular").value;
    nuevoEmpleado.nacimiento = document.getElementById("idNacimiento").value;
    nuevoEmpleado.clave = document.getElementById("idClave").value;
    nuevoEmpleado.puesto = document.getElementById("idPuesto").value;

    if (validarCampoObligatorio(nuevoEmpleado.nombre) == true && validarCampoObligatorio(nuevoEmpleado.apellido)==true && validarCampoObligatorio(nuevoEmpleado.dni)==true && validarCampoObligatorio(nuevoEmpleado.domicilio)==true && validarCampoObligatorio(nuevoEmpleado.clave)==true && validarCampoObligatorio(nuevoEmpleado.puesto)==true) {
        statusCamposObligatorios = true;
    }
 
    if (statusCamposObligatorios) {
        if(localStorage.getItem("empleadoStorage"))
        {
            empleadoList = JSON.parse(localStorage.getItem("empleadoStorage")); //Convierte string a lo que es
            statusNuevoEmpleado = validarNuevoEmpleado(empleadoList, nuevoEmpleado.dni);
            if (statusNuevoEmpleado == false) {
                alert("Empleado existente. Intente nuevamente");
                location.reload();
                return;
            }
        }
        
        empleadoList.push(nuevoEmpleado); //agrego el nuevo usuario a la lista
    
        var empleadoListStr = JSON.stringify(empleadoList); //Convierte un objeto a String 
    
        localStorage.setItem("empleadoStorage", empleadoListStr);
    
        alert("Nuevo empleado creado exitosamente");
        window.location.href="grillaEmpleado.html";
        
    } else {
        alert("Debe completar los campos obligatorios");
    }    
}

function validarNuevoEmpleado(lista, dni) {
    let validacionStatus = true;

    lista.forEach(element => {
        if (element.dni == dni.toUpperCase()) {
            validacionStatus = false;   
        }

    });

    return validacionStatus;
}


document.addEventListener("DOMContentLoaded", function(event){
    cargaPuesto();
});