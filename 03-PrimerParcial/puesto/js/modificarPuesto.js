let url = "../../index.html";
getLogin(url);

let viejoPuesto = {};
let puestoList = JSON.parse(localStorage.getItem("puestoStorage"));
let empleadoList = JSON.parse(localStorage.getItem("empleadoStorage"));

function loadModifyForm() {
    let viejoPuestoList = [];
    viejoPuestoList = JSON.parse(localStorage.getItem("auxStorage"));
   

    viejoPuestoList.forEach(element => {
        viejoPuesto.posicion = element.posicion;
        viejoPuesto.abreviatura = element.abreviatura;
        viejoPuesto.puesto = element.puesto;
    });
    
    localStorage.removeItem("auxStorage");
    
    document.getElementById("idViejoPuesto").value = viejoPuesto.puesto;
    document.getElementById("idViejaAbreviatura").value = viejoPuesto.abreviatura;
}

function modificarPuesto() {
    let posicion = viejoPuesto.posicion;
	let nuevoPuesto = {};
    nuevoPuesto.abreviatura = document.getElementById("idNuevaAbreviatura").value;
    nuevoPuesto.puesto = document.getElementById("idNuevoPuesto").value;
    
    if (nuevoPuesto != null) {
        puestoList[posicion] = nuevoPuesto;
        let puestoListStr = JSON.stringify(puestoList);
        localStorage.setItem("puestoStorage", puestoListStr);

        updatePuestoEnEmpleados(nuevoPuesto.puesto, viejoPuesto.puesto, nuevoPuesto.abreviatura);

        alert("Puesto modificado exitosamente");

        window.location.href="grillaPuesto.html"
    } else {
        alert("Ingrese un nombre de puesto");     
    }
}

function updatePuestoEnEmpleados(nuevoPuesto, viejoPuesto, nuevaAbreviatura) {
    empleadoList.forEach(element => {
        if (viejoPuesto == element.puesto) {
            element.puesto = nuevoPuesto;
            element.abreviatura = nuevaAbreviatura;
        }
    });

    let empleadoListStr = JSON.stringify(empleadoList);
    localStorage.setItem("empleadoStorage", empleadoListStr);
}

document.addEventListener("DOMContentLoaded", function(event) {
    loadModifyForm();
  });