let url = "../../index.html";
getLogin(url);

function crearPuesto() {
    let nuevoPuesto = {};
    let puestoList = [];
    let statusnuevoPuesto = true;

    nuevoPuesto.abreviatura = document.getElementById("idPuesto").value;
    nuevoPuesto.puesto = document.getElementById("idPuesto").value;       
        
    if(localStorage.getItem("puestoStorage"))
    {
        puestoList = JSON.parse(localStorage.getItem("puestoStorage"));
        statusnuevoPuesto = validarNuevoPuesto(puestoList, nuevoPuesto.puesto, nuevoPuesto.abreviatura);
        if (statusnuevoPuesto == false) {
            alert("Perfil existente. Intente nuevamente");
            return;
        } 
    }
    
    puestoList.push(nuevoPuesto);

    let puestoListStr = JSON.stringify(puestoList);

    localStorage.setItem("puestoStorage", puestoListStr);

    alert("Perfil creado exitosamente");

    window.location.href="grillaPuesto.html";
}

function validarNuevoPuesto(list, nuevoPuesto, nuevaAbreviatura) {
    let validationStatus = true;

    list.forEach(element => {
        if (element.puesto.toUpperCase() == nuevoPuesto.toUpperCase()) {
            if (element.abreviatura.toUpperCase() == nuevaAbreviatura.toUpperCase()){
                validationStatus = false;   

            }
        }

    });

    return validationStatus;
}