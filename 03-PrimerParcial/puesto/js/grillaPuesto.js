let url = "../../index.html";
getLogin(url);

let puestoList = JSON.parse(localStorage.getItem("puestoStorage"));
let empleadoList = JSON.parse(localStorage.getItem("empleadoStorage"));

function cargarGrillaPuesto() {
    if(localStorage.getItem("puestoStorage")){
        let tbody = document.getElementById("idTBody");
                        
        puestoList.forEach((element,index) => {
            let row = tbody.insertRow(index);
            let cellAbreviatura = row.insertCell(0);
            let cellPuesto = row.insertCell(1);
            let cellModify = row.insertCell(2);
            let cellDelete = row.insertCell(3);
            cellAbreviatura.innerHTML = element.abreviatura;
            cellPuesto.innerHTML = element.puesto;
            cellModify.innerHTML = '<input class="btnModify"  type="button" onclick="goToModifyForm('+ index + ')" value="Modificar" />';
            cellDelete.innerHTML = '<input class="btnDelete"  type="button" onclick="deletePuesto('+ index + ')" value="Eliminar" />';
            
        });
        
    } 
    
}



function deletePuesto(posicion) {
    
    if (validatePuestoToDelete(posicion)) {
        puestoList.splice(posicion,1);
        localStorage.setItem('puestoStorage', JSON.stringify(puestoList));
        
        location.reload();
        
    } else {
        alert("Perfil en uso. No puede ser eliminado");
    }
}


function validatePuestoToDelete(posicion) {
    let status = true;

    empleadoList.forEach(element => {
        if (puestoList[posicion].puesto == element.puesto) {
            status = false;
        }  
    });
    return status;
}


function goToModifyForm(posicion) {
    let respuesta = false;
    let viejoPuesto = {};
    let viejoPuestoList = [];
    
    viejoPuesto.posicion = posicion;
    viejoPuesto.abreviatura = puestoList[posicion].abreviatura;
    viejoPuesto.puesto = puestoList[posicion].puesto;
        
    viejoPuestoList.push(viejoPuesto);
    let viejoPuestoListStr = JSON.stringify(viejoPuestoList);
    localStorage.setItem("auxStorage", viejoPuestoListStr);
  
    respuesta = window.confirm("¿ Desea modificar el perfil "+ viejoPuesto.puesto + "?");
    
    if (respuesta) {
        window.location.href = "modificarPuestoForm.html"; 
    } else {
        localStorage.removeItem("auxStorage");
    }
}


document.addEventListener("DOMContentLoaded", function(event) {
    cargarGrillaPuesto();
});
