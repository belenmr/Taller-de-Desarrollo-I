var userInput = {}
userInput.username;
userInput.pass;


function login() {
    userInput.username = document.getElementById("idUser");
    userInput.pass = document.getElementById("idPass");



    if (validateUser(userInput.username.value, userInput.pass.value)) {
        alert("Usuario y clave correctas");
        loginOk();
        window.location.href="user/views/gridView.html";
    } else {
        alert("Usuario y/o clave incorrectas");
    }
}

function loginOk() {
    //Almacena la informacion en sessionStorage
    sessionStorage.setItem('LoginOk','OK');
}

function goToRegister() {
    window.location.href="user/views/registerForm.html";
}

function validateUser(username, password) {
    let validationStatus = false;
    let userList = [];

    userList = JSON.parse(localStorage.getItem("userStorage"));

    userList.forEach(element => {
        if (element.name == username && element.pass == password) {
            validationStatus = true;     
        }
        });

    return validationStatus;
}