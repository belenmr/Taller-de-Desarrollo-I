function signup() {
    let newUser = {};
    let userList = [];
    let statusNewUser = true;

    newUser.name = document.getElementById("idUsername").value;    
    //newUser.email = document.getElementById("idEmail").value;    
    newUser.pass = document.getElementById("idPass").value;
    newUser.profile = document.getElementById("idProfileOption").value;
        
    if(localStorage.getItem("userStorage"))
    {
        userList = JSON.parse(localStorage.getItem("userStorage")); //Convierte string a lo que es
        //console.log(userList)
        statusNewUser = validateNewUsername(userList, newUser.name);
        if (statusNewUser == false) {
            alert("Usuario existente. Intente nuevamente");
            return;
        } 
    }
    
    userList.push(newUser); //agrego el nuevo usuario a la lista

    var newUserStr = JSON.stringify(userList); //Convierte un objeto a String 

    localStorage.setItem("userStorage", newUserStr);

    alert("Nuevo usuario creado exitosamente");
    window.location.href="../.../index.html";
}

function validateNewUsername(list, newUsername) {
    let validationStatus = true;

    list.forEach(element => {
        if (element.name.toUpperCase() == newUsername.toUpperCase()) {
            validationStatus = false;   
        }

    });

    return validationStatus;
}

function loadProfileOptions() {
    let select = document.getElementById("idProfileOption");
    let profileList = [];
    
    if (localStorage.getItem("profileStorage") == null) {
        let profile = "Administrador";
        profileList.push(profile);
        let profileListStr = JSON.stringify(profileList);
        localStorage.setItem("profileStorage", profileListStr);
    }

    profileList = JSON.parse(localStorage.getItem("profileStorage"));
    
    profileList.forEach(element => {
        let option = document.createElement("option");
        option.text = element;
        select.appendChild(option);  
    });
}

document.addEventListener("DOMContentLoaded", function(event){
    loadProfileOptions();
});