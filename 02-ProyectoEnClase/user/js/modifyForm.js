let oldUser = {};
var userList = JSON.parse(localStorage.getItem("userStorage"));
function loadModifyForm() {
    let oldUserList = [];
    oldUserList = JSON.parse(localStorage.getItem("auxStorage"));
   

    oldUserList.forEach(element => {
        oldUser.position = element.position;
        oldUser.name = element.name;
        oldUser.pass = element.pass;

    });
    
    localStorage.removeItem("auxStorage");
    
    document.getElementById("idOldUsername").value = oldUser.name;
    document.getElementById("idOldPass").value = oldUser.pass;  
}

function modifyUser() {
    let position = oldUser.position;
    let newUser = document.getElementById("idNewUsername").value;
    let newPass = document.getElementById("idNewPass").value;
    
    if (newUser != null && newPass != null) {
        userList[position].name = newUser;
        userList[position].pass = newPass;

        alert("Usuario y contraseña modificados exitosamente");
    } else {
        if (newUser != null && newPass == null) {
            userList[position].name = newUser;
            alert("Usuario modificado exitosamente");
        }

        if (newUser == null && newPass != null) {
            userList[position].pass = newPass;
            alert("Contraseña modificada exitosamente");
        }
    }

    let listUserStr = JSON.stringify(userList);

    localStorage.setItem("userStorage", listUserStr);

    window.location.href="gridView.html";

}

document.addEventListener("DOMContentLoaded", function(event) {
    loadModifyForm();
  });