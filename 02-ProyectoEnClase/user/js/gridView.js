let userList = JSON.parse(localStorage.getItem("userStorage"));
let oldUser = "";
let oldPass = "";

function loadUserGrid() {
    if(localStorage.getItem("userStorage")){
        let tbody = document.getElementById("gridBody");
        //console.log(tbody);
                
        userList.forEach((element,index) => {
            let row = tbody.insertRow(index);
            let cellUser = row.insertCell(0);
            let cellPass = row.insertCell(1);
            let cellProfile = row.insertCell(2);
            let cellModify = row.insertCell(3);
            let cellDelete = row.insertCell(4);
            cellUser.innerHTML = element.name;
            cellPass.innerHTML = element.pass;
            cellProfile.innerHTML = element.profile;
            cellModify.innerHTML = '<input class="btnModify"  type="button" onclick="goToModifyForm('+ index + ')" value="Modificar" />';
            cellDelete.innerHTML = '<input class="btnDelete"  type="button" onclick="deleteUser('+ index + ')" value="Eliminar" />';
            
        });
        
    } 
    else {
        alert("No hay usuarios registrados");
    }
}


/*Si no esta ok la sesion, te manda de nuevo a la pagina login
* Para que no se pueda acceder copiando la url
*/
function getLogin() {
    var data = sessionStorage.getItem('LoginOk');
    verifyLogin(data);
}

getLogin();

function verifyLogin(Data) {
    /*
    if (Data == "OK") {
        alert("Todo bien");
    } 
    else{
        //redirecciona a la pagina indicada
        window.location.href="../index.html";
    }*/
    if (Data != "OK") {
        window.location.href="../../index.html";
    }
}

function logout() {
    //Manera no optima
    //sessionStorage.setItem('LoginOk', 'NotOK');

    //Manera más optima de borrar VS
    sessionStorage.removeItem('LoginOk');
    window.location.href="../../index.html";
    
}

function deleteUser(position) {
    userList.splice(position,1);
    localStorage.setItem('userStorage', JSON.stringify(userList));
    
    location.reload();
}

function goToModifyForm(position) {
    let answer = false;
    let oldUser = {};
    let oldUserList = [];
    
    oldUser.position = position;
    oldUser.name = userList[position].name;
    oldUser.pass = userList[position].pass;
    
    oldUserList.push(oldUser);
    let oldUserStr = JSON.stringify(oldUserList);
    localStorage.setItem("auxStorage", oldUserStr);
  
    answer = window.confirm("¿ Desea modificar el usuario "+ oldUser.name + "?");
    
    if (answer) {
        window.location.href = "modifyForm.html"; 
    } else {
        localStorage.removeItem("auxStorage");
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    loadUserGrid()
  });