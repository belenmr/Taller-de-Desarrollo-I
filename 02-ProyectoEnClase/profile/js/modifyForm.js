let oldProfile = {};
let profileList = JSON.parse(localStorage.getItem("profileStorage"));
let userList = JSON.parse(localStorage.getItem("userStorage"));

function loadModifyForm() {
    let oldProfileList = [];
    oldProfileList = JSON.parse(localStorage.getItem("auxStorage"));
   

    oldProfileList.forEach(element => {
        oldProfile.position = element.position;
        oldProfile.name = element.name;
    });
    
    localStorage.removeItem("auxStorage");
    
    document.getElementById("idOldProfile").value = oldProfile.name;
}

function modifyProfile() {
    let position = oldProfile.position;
    let newProfile = document.getElementById("idNewProfile").value;
    
    if (newProfile != null) {
        profileList[position] = newProfile;
        let profileListStr = JSON.stringify(profileList);
        localStorage.setItem("profileStorage", profileListStr);

        updateProfileInUsers(newProfile, oldProfile.name);

        alert("Perfil modificado exitosamente");

        window.location.href="gridView.html"
    } else {
        alert("Ingrese un nombre de perfil");     
    }
}

function updateProfileInUsers(newProfile, oldProfile) {
    userList.forEach(element => {
        if (oldProfile == element.profile) {
            element.profile = newProfile;
        }
    });

    let userListStr = JSON.stringify(userList);
    localStorage.setItem("userStorage", userListStr);
}

document.addEventListener("DOMContentLoaded", function(event) {
    loadModifyForm();
  });