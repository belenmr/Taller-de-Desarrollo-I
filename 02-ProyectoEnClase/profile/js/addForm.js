function createProfile() {
    let newProfile = "";
    let profileList = [];
    let statusNewProfile = true;

    newProfile = document.getElementById("idProfile").value;       
        
    if(localStorage.getItem("profileStorage"))
    {
        profileList = JSON.parse(localStorage.getItem("profileStorage"));
        statusNewProfile = validateNewProfile(profileList, newProfile);
        if (statusNewProfile == false) {
            alert("Perfil existente. Intente nuevamente");
            return;
        } 
    }
    
    profileList.push(newProfile);

    let profilesStr = JSON.stringify(profileList);

    localStorage.setItem("profileStorage", profilesStr);

    alert("Perfil creado exitosamente");

    window.location.href="gridView.html";
}

function validateNewProfile(list, profile) {
    let validationStatus = true;

    list.forEach(element => {
        if (element.toUpperCase() == profile.toUpperCase()) {
            validationStatus = false;   
        }

    });

    return validationStatus;
}