let profileList = JSON.parse(localStorage.getItem("profileStorage"));
let userList = JSON.parse(localStorage.getItem("userStorage"));

function loadProfileGrid() {
    if(localStorage.getItem("profileStorage")){
        let tbody = document.getElementById("gridBody");
                        
        profileList.forEach((element,index) => {
            let row = tbody.insertRow(index);
            let cellProfile = row.insertCell(0);
            let cellModify = row.insertCell(1);
            let cellDelete = row.insertCell(2);
            cellProfile.innerHTML = element;
            cellModify.innerHTML = '<input class="btnModify"  type="button" onclick="goToModifyForm('+ index + ')" value="Modificar" />';
            cellDelete.innerHTML = '<input class="btnDelete"  type="button" onclick="deleteProfile('+ index + ')" value="Eliminar" />';
            
        });
        
    } 
    
}


/*Si no esta ok la sesion, te manda de nuevo a la pagina login
* Para que no se pueda acceder copiando la url
*/
function getLogin() {
    var data = sessionStorage.getItem('LoginOk');
    verifyLogin(data);
}

getLogin();

function verifyLogin(Data) {
    if (Data != "OK") {
        window.location.href="../../index.html";
    }
}


function deleteProfile(position) {
    
    if (validateProfileToDelete(position)) {
        profileList.splice(position,1);
        localStorage.setItem('profileStorage', JSON.stringify(profileList));
        
        location.reload();
        
    } else {
        alert("Perfil en uso. No puede ser eliminado");
    }
}


function validateProfileToDelete(position) {
    let status = true;

    userList.forEach(element => {
        if (profileList[position] == element.profile) {
            status = false;
        }  
    });
    return status;
}


function goToModifyForm(position) {
    let answer = false;
    let oldProfile = {};
    let oldProfileList = [];
    
    oldProfile.position = position;
    oldProfile.name = profileList[position];
        
    oldProfileList.push(oldProfile);
    let oldProfileStr = JSON.stringify(oldProfileList);
    localStorage.setItem("auxStorage", oldProfileStr);
  
    answer = window.confirm("¿ Desea modificar el perfil "+ oldProfile.name + "?");
    
    if (answer) {
        window.location.href = "modifyForm.html"; 
    } else {
        localStorage.removeItem("auxStorage");
    }
}


document.addEventListener("DOMContentLoaded", function(event) {
    loadProfileGrid();
});
