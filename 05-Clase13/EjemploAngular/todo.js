angular.module('todoApp', [])
	.controller('TodoListController', function () {
		var todoList = this;
		todoList.todos = [
			{ text: 'learn AngularJS', done: true },
			{ text: 'build an AngularJS app', done: false }];

		todoList.addTodo = function () {
			todoList.todos.push({ text: todoList.todoText, done: false });
			todoList.todoText = '';
		};

		todoList.remaining = function () {
			var count = 0;
			angular.forEach(todoList.todos, function (todo) {
				count += todo.done ? 0 : 1;
			});
			return count;
		};

		todoList.archive = function () {
			var oldTodos = todoList.todos;
			todoList.todos = [];
			angular.forEach(oldTodos, function (todo) {
				if (!todo.done) todoList.todos.push(todo);
			});
		};

		todoList.usuarios = [
			{nombre: "Nicolas", apellido: "Gomez"},
			{nombre: "build an AngularJS app", apellido: "false"} ];
		
		/*
		todoList.usuarios = [];
		var usuario = {nombre: "Nicolas", apellido: "Gomez"};
		todoList.usuarios.push(usuario);
		*/

		todoList.showFormulario = false;
        todoList.soyAlta = false;

		todoList.addUsuario = function() {
			var usuario = {nombre: todoList.NewUserNombre, apellido: todoList.NewUserApellido};
			todoList.usuarios.push(usuario);
		};

		todoList.delUsuario = function (index) {
            todoList.usuarios.splice(index, 1);
		};
		
		todoList.indexAEditar = 0;

        todoList.ModificarUsuario = function (usuario, index) {
            todoList.showFormulario = true;
            todoList.soyAlta = false;
            todoList.indexAEditar = index;
            todoList.NewUserNombre = usuario.nombre;
            todoList.NewUserApellido = usuario.apellido;
        };

        todoList.SaveModificarUsuario = function () {

            todoList.usuarios[todoList.indexAEditar].nombre = todoList.NewUserNombre;
            todoList.usuarios[todoList.indexAEditar].apellido = todoList.NewUserApellido;
        };

        todoList.nuevoUsuario = function () {
            todoList.NewUserNombre = "";
            todoList.NewUserApellido = "";
            todoList.showFormulario = true;
             todoList.soyAlta = true;
        };

	});